# LEO

LEO is a [Gemini](https://gemini.circumlunar.space/) server written in PHP.

## Setup

LEO is currently configuration free. It requires port `1965` to be open.

### Steps

Copy `leo.php` into a directory where you want you `capsules` directory to be and run it.

```
php leo.php
```

This will automatically create the `capsules` directory and provide further instructions.

Once one or more capsules exist LEO will serve them when run.

### Example

An example of the required file structure is as follows.

```
gemini/
	leo.php
	capsules/
		localhost.crt
		localhost/
			index.gmi
			...
		example.tld.crt
		example.tld/
			index.gmi
			...
```

This approach is stolen directly from [ergol](https://codeberg.org/adele.work/ergol). Thank you, adele.

## Missing

Known missing functionality I plan to implement includes the following.

- Logging of server events.
- Beginning `leo.php` with configurable constants.
- Handling of unlikely PHP errors.
- Interactive capsule creation.
