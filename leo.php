#!/usr/bin/php

<?php

if ( ! is_dir("./capsules") and ! mkdir("./capsules")) {
  $mkdirFail = "Failed to create missing 'capsules' directory.\n";
  if ( ! is_writable(".")) {
    $mkdirFail .= "Please ensure LEO has write permission for '"
      .realpath(".")."'\n";
  }
  exit($mkdirFail);
}

function getCertInstructions($host) {
  return <<<CERT

  openssl req -x509 -newkey rsa:4096 -days 730 -nodes \
  -subj "/CN=$host"          \
  -keyout capsules/$host.crt \
  -out capsules/$host.crt

  CERT;
}

$capsules = array_diff(
  scandir("./capsules"),
  ['.', '..']
);
$certs = Array();

foreach ($capsules as $file) {
  if ( ! is_dir("./capsules/".$file)) { continue; }
  if ( ! is_file("./capsules/".$file.'.crt')) {
    echo <<<CERT
    Missing required "capsules/$file.crt" file.

    Create it by running the following:

    CERT.getCertInstructions($file);
    continue;
  }
  $certs[$file] = "./capsules/".$file.'.crt';
}
if (empty($capsules)) {
  exit(
    <<<HOST
    No capsules found. Please create one.

    For example, to create a capsule served to localhost,
    create the directory "capsules/localhost".
    
    Next, create an SSL certificate by running the following:
    
    HOST.getCertInstructions("localhost")
  );
}
if (empty($certs)) { exit(); }

$context = stream_context_create(
  array(
    'ssl' => array(
      'verify_peer'       => false,
      'allow_self_signed' => true,
      'SNI_enabled'       => true,
      'SNI_server_certs'  => $certs
    )
  )
);

$server = stream_socket_server(
  'tcp://[::]:1965',
  $errno,  // set to error number
  $errstr, // set to error description
  STREAM_SERVER_BIND | STREAM_SERVER_LISTEN,
  $context
);
// handle server create fail
if ($server === false) {
  print_r($errno);
  print_r($errstr);
  exit();
}

while (true) {
  $stream = stream_socket_accept(
    $server,
    -1,      // never timeout
    $client  // set to client name
  );
  // handle stream create fail

  $ssbStatus = stream_set_blocking($stream, true);
  // handle ssb fail
  $ssecStatus = stream_socket_enable_crypto(
    $stream,
    true,
    STREAM_CRYPTO_METHOD_TLS_SERVER
  );
  // handle ssec fail
  $request = fread($stream, 1030);
  // handle fread fail
  $ssbStatus = stream_set_blocking($stream, false);
  // handle ssb fail

  $url = parse_url(trim($request));
  // handle badly malformed urls

  $capsule = strtolower($url['host']);
  $path = './capsules/'.$capsule
    .($url['path'] ?? '/');

  $done = function($response) use (&$stream, &$client, &$path) {
    $header = strtok($response, "\n");
    echo $client." ".$path." ".$header."\n";
    fwrite($stream, $response."\r\n");
    fclose($stream);
  };

  if (isRequestBad($request)) {
    $done("59 BAD REQUEST");
    continue;
  }

  if (isRequestProxied($url, $certs)) {
    $done("53 PROXY REQUEST REFUSED");
    continue;
  }

  if (is_dir($path)) {
    $path = getGem($path, 'index.{gemini,gmi}');
  }
  elseif ( ! file_exists($path)) {
    $path = getGem($path, '{.gemini,.gmi}');
  }

  if ( ! is_file($path)) {
    $done("51 NOT FOUND");
    continue;
  }

  $ext = pathinfo($path, PATHINFO_EXTENSION)
    ?: '';

  if (
    is_executable($path) and !
    in_array($ext, ['gemini', 'gmi'])
  ) {
    $query = $url['query'] ?? '';
    $cmd = './'.$path.' '.$query;

    exec($cmd, $output, $code);
    // handle error when code !== 0

    $response = implode("\r\n", $output);
    unset($output);
    
    $done($response);
    continue;
  }

  $mime = in_array($ext, ['gemini', 'gmi', ''])
      ? 'text/gemini'
      : mime_content_type($path);

  $content = file_get_contents($path);
  $response = "20 ".$mime."\r\n".$content;

  $done($response);
  continue;
}

function isRequestBad($request) {
  $url = parse_url(trim($request));
  return ! (
    ! empty($url) and
    ! empty($url['scheme']) and
    ! strpos($request, "../") and
    strlen(trim($request)) <= 1024 and
    mb_check_encoding($request, "UTF-8") and
    substr_compare($request, "\r\n", -2) === 0
  );
}

function isRequestProxied($url, $certs) {
  return (
    $url['scheme'] !== 'gemini' or
    empty($certs[$url['host']]) or
    (
      ! empty($url['port']) and
      $url['port'] !== 1965
    )
    );
}

function getGem($path, $glob) {
  $matches = glob($path.$glob, GLOB_BRACE);
  return (is_array($matches) and ! empty($matches))
    ? $matches[0]
    : $path;
}

?>
